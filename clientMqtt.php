<?php
require_once 'autoloader.php';

use PlugisDashboard\MQTT\Donnees\GestionValeurInstantane;
use PlugisDashboard\MQTT\Parser\PlugisMQTTParser;
use PlugisDashboard\MQTT\Parser\Exceptions\ParsingException;

$client = new Mosquitto\Client();
$client->onConnect('connect');
$client->onDisconnect('disconnect');
$client->onSubscribe('subscribe');
$client->onMessage('message');
$client->connect("test.mosquitto.org", 1883);
$client->subscribe('plugis', 1);


$client->loopForever();

$client->disconnect();
unset($client);

function connect($r) {
    echo "I got code {$r}\n";
}

function subscribe() {
    echo "inscription au au sujet plugis\n";
}

function message($message) {
    $contenu = $message->payload;
    
    echo "Message MQTT reçu : $contenu" . PHP_EOL;
    
    
    echo "Parsing du message..." . PHP_EOL;
    $parser = new PlugisMQTTParser();
    try {
        
        $valeurInstantanee = $parser->parse($contenu);
        

        echo "Parsing effectué avec succes" . PHP_EOL;
        
        echo "Stockage du message reçu" . PHP_EOL;
        $gestionValeurInstantannee = new GestionValeurInstantane();
        $gestionValeurInstantannee->insertDonnees($valeurInstantanee);
    } catch (ParsingException $exc) {
        
        echo "Erreur de parsing du message reçu: " . $exc->getMessage() . PHP_EOL;
    }
    echo "Message traité avec succes" . PHP_EOL;

    
}

function disconnect() {
    echo "Deconnection d'MQTT\n";
}