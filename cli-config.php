<?php
# cli-config.php
require_once 'autoloader.php';
$entityManager = require_once join(DIRECTORY_SEPARATOR, [DIR_RACINE, 'bootstrap.php']);

use Doctrine\ORM\Tools\Console\ConsoleRunner;

return ConsoleRunner::createHelperSet($entityManager);

