<?php
error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set("html_errors", 0);

// les path pour acceder au données config et constante
define("DIR_RACINE", __DIR__);

function autoloader($dir) {
    // require all php files
    $scan = glob("$dir/*");
    foreach ($scan as $path) {
        if (preg_match('/\.php$/', $path)) {
            require_once $path;
        }
        elseif (is_dir($path)) {
            autoloader($path);
        }
    }
}

autoloader(DIR_RACINE."/PlugisDashboard");

//echo 'autoloader success !';

//charger Doctrine
//require_once DIR_RACINE."/bootstrap.php";
