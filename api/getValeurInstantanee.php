<?php

/**
 * Fichier représentant un WebService étant appeler par exemple
 * par le JavaScript d'une page
 * 
 * Par exemple:
 * 
 * Javascript fait un requete GET sur l'adresse 
 *              http://localhost/api/getValeurInstantanee.php?type=9.001
 */

require_once '../autoloader.php';

use PlugisDashboard\MQTT\Donnees\GestionValeurInstantane;


define("DT_PARAM","dt");
define("AG_PARAM","ag");

/**
 *
 */
if(isset($_GET[DT_PARAM]) && $_GET[DT_PARAM] != ""
        && isset($_GET[AG_PARAM]) && $_GET[AG_PARAM] != ""){
    $dataManager = new GestionValeurInstantane();
    //
    $requestData = $dataManager->getByAdresseGroupeAndDt($_GET[AG_PARAM], $_GET[DT_PARAM]);
    // renvoie des données en json
    $json = json_encode($requestData);
    echo $json;
}
else {
    echo "400 Bad Request";
}

