<?php

namespace PlugisDashboard\MQTT\Donnees;


class EntityManagerProvider {

    private static $_instance;

    private $entityManager;
    /**

+     * 

+     * @return type EntityManagerProvider

+     */

public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new EntityManagerProvider();
        }
        return self::$_instance;
    }
  /**

+    * Le constrcuteur avec sa logique est privé pour émpêcher l'instanciation en dehors de la classe

+    **/

private function __construct(){

        $this->entityManager = require_once join(DIRECTORY_SEPARATOR, [DIR_RACINE, 'bootstrap.php']);

    }

public function getEntityManager() {

        return $this->entityManager;
    }
}

