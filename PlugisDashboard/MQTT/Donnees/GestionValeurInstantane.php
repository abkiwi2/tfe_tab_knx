<?php

namespace PlugisDashboard\MQTT\Donnees;

class GestionValeurInstantane {

    private $valeurInstantaneRepo;
    private $entityManager;

    function __construct() {
        //$this->entityManager = require_once "EntityManagerProvider.php"
        $this->entityManager = EntityManagerProvider::getInstance()->getEntityManager();
        $this->valeurInstantaneRepo = $this->entityManager->getRepository(\ValeurInstantane::class);
    }

    /*
     * insert des valeurs via doctrine dans la Base de données
     */

    public function insertDonnees($valeurInstantane) {
        $this->entityManager->persist($valeurInstantane);
        $this->entityManager->flush();
    }

    /**
     * méthodes
     */
    function getByType($dataPointType) {

        return $this->valeurInstantaneRepo->findBy(["dataPointType" => $dataPointType]);
//        echo "valeur instantaner par datapoint type:\n";
//        foreach ($valInstByDt as $valeurInstantane) {
//            echo $valeurInstantane;
//        }
    }

    function getByAdresseGroupe($adresseGroupe) {

        return $this->valeurInstantaneRepo->findBy(["adresseGroupe" => $adresseGroupe]);
//        echo "valeur instantaner par adresse de groupe:\n";
//        foreach ($valInstByAdres as $valeurInstantane) {
//            echo $valeurInstantane;
//        }
    }

    function getByAdresseGroupeAndDt($adresseGroupe, $dataPointType) {
        return $this->valeurInstantaneRepo->findBy(["adresseGroupe" => $adresseGroupe, "dataPointType" => $dataPointType]);
//        echo "valeur instantaner par adresse de groupe et types de donner:\n";
//        foreach ($valInstByAdresByDt as $valeurInstantane) {
//            echo $valeurInstantane;
//        }
    }

}
