<?php
namespace PlugisDashboard\MQTT\Parser;

abstract class AbstractParser {

    public abstract function parse($str = "");
}