<?php

namespace PlugisDashboard\MQTT\Parser;
use PlugisDashboard\MQTT\Parser\Exceptions\ParsingException;

class PlugisMQTTParser extends AbstractParser {

    const BOOL = "bool";
    const TYPE = "type";
    const PRINCIPAL = "principal";
    const MEDIAN = "median";
    const NUM = "num";
    const VALUE = "value";
    const PATTERN = "@"
            . "(?P<" . self::BOOL . ">true|false),"
            . "(?P<" . self::TYPE . ">\d+[.]\d+),"
            . "(?P<" . self::PRINCIPAL . ">\d+)/"
            . "(?P<" . self::MEDIAN . ">\d+)/"
            . "(?P<" . self::NUM . ">\d+),"
            . "(?P<" . self::VALUE . ">\d+([.]\d+)?)"
            . "@";

    public function parse($str = "") {
        if (preg_match(self::PATTERN, $str, $matches)) {
            $idValeurInstannee = null; // c'est censé être un id autoincrément donc je ne sais pas quoi mettre comme valeur
            $heurePriseValeur = new \DateTime();
            $statuCommande = filter_var($matches[self::BOOL], FILTER_VALIDATE_BOOLEAN);
            $dataPointType = $matches[self::TYPE];
            $adresseGroupe = $matches[self::PRINCIPAL] . '/' . $matches[self::MEDIAN] . '/' . $matches[self::NUM];
            $valeur = $matches[self::VALUE];
            return new \ValeurInstantane($idValeurInstannee, $heurePriseValeur, $statuCommande, $dataPointType, $adresseGroupe, $valeur);
        }
        throw new ParsingException();
    }

}
