<?php
//namespace PlugisDashboard\MQTT\Domain;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="valeurinstantane")
*/
class ValeurInstantane
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $idValeurInstantane;
    /**
     *@ORM\Column(type="datetime")
     */
    protected $heurePriseValeur;
    /**
     *@ORM\Column(type="boolean")
     */
    protected $statuCommande;
    /**
     *@ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $dataPointType;
    /**
     *@ORM\Column(type="string", length=11)
     */
    protected $adresseGroupe;
    /**
     *@ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $valeur;
    
    function __construct($idValeurInstantane,$heurePriseValeur,$statuCommande, $dataPointType, $adresseGroupe, $valeur) {
        $this->idValeurInstantane = $idValeurInstantane;
        $this->heurePriseValeur = $heurePriseValeur;
        $this->statuCommande = $statuCommande;
        $this->dataPointType = $dataPointType;
        $this->adresseGroupe = $adresseGroupe;
        $this->valeur = $valeur;
        //$this->valeurInstantane = new ArrayCollection();
    }
    
    public function getIdValeurInstantane()
    {
        return $this->idValeurInstantane;
    }

    public function setIdValeurInstantane($idValeurInstantane)
    {
        $this->idValeurInstantane = $idValeurInstante;
    }
    public function getHeurePriseValeur()
    {
        return $this->heurePriseValeur;
    }

    public function setHeurePriseValeur($heurePriseValeur)
    {
        $this->heurePriseValeur = $heurePriseValeur;
    }
public function getStatuCommande()
    {
        return $this->statuCommande;
    }

    public function setStatuCommande($statuCommande)
    {
        $this->statuCommande = $statuCommande;
    }
    public function getDataPointType()
    {
        return $this->dataPointType;
    }

    public function setDataPointType($dataPointType)
    {
        $this->dataPointType = $dataPointType;
    }
    public function getAdresseGroupe()
    {
        return $this->adresseGroupe;
    }

    public function setAdresseGroupe($adresseGroupe)
    {
        $this->adresseGroupe = $adresseGroupe;
    }
    public function getValeur()
    {
        return $this->valeur;
    }

    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }
    public function jsonSerialize() {
        return get_object_vars($this);
    }

    /*public function __toString(){
        $format = "User (id: %s, firstname: %s, lastname: %s, role: %s)\n";
        return sprintf($format, $this->id, $this->firstname, $this->lastname, $this->role);
    }*/
}

