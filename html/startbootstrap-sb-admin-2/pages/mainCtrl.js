var TESTALEX = angular.module('TESTALEX', ['tc.chartjs']);

TESTALEX.controller('mainCtrl', ['$scope',
    function ($scope) {
        $scope.tab = [
            {
                date : 'Jan 2000',
                price : '1394.46'
            },
            {
                date : 'Feb 2000',
                price : '1366.46'
            },{
                date : 'Mar 2000',
                price : '1498.46'
            },{
                date : 'Apr 2000',
                price : '1456.46'
            },{
                date : 'May 2000',
                price : '1752.46'
            },{
                date : 'Jun 2000',
                price : '1384.46'
            },{
                date : 'Jul 2000',
                price : '1423.46'
            },{
                date : 'Aug 2000',
                price : '1152.46'
            },{
                date : 'Sep 2000',
                price : '1452.46'
            },{
                date : 'Oct 2000',
                price : '1315.46'
            },{
                date : 'Nov 2000',
                price : '1278.46'
            },{
                date : 'Dec 2000',
                price : '1842.46'
            }
        ];        
        
        $scope.choixDate = {
            min : $scope.tab[0].date,
            max : $scope.tab[$scope.tab.length-1].date
        };
        
        $scope.choixIndice = {
            min : 0,
            max : $scope.tab.length-1
        };
        
        
        $scope.majChoixIndice = function(grandeur){
             var l_tab= $scope.tab.length;
            for (var i =0;i<=l_tab;i++){
                if ($scope.tab[i].date === $scope.choixDate[grandeur]){
                    $scope.choixIndice[grandeur] = i;
                    break;
                }
            }
        };
        
        $scope.majChart = function(grandeur){
           $scope.majChoixIndice(grandeur);
           
           var tabDate = [];
           var tabPrice = [];
           var displayTabData = [];
           
           //garde les elem du tab de base compris entre le select min et le select max
           for(var i=$scope.choixIndice.min; i<=$scope.choixIndice.max;i++){
               tabDate.push($scope.tab[i].date);
               tabPrice.push($scope.tab[i].price);
               displayTabData.push({
                           date : $scope.tab[i].date,
                           price:$scope.tab[i].price
                       });
           };
           
           //structure de data pour le graph
           var data = {
            labels: tabDate,
            datasets: [
              {
                label: "Price",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: tabPrice,
                spanGaps: false,
              }
            ]
          };
          
          
          
          //actualise le graph et le tableau
          $scope.chartData = data;
          $scope.displayTabData = displayTabData;
        };
        
         

        $scope.myOptions = {
            // Chart.js options go here
            // e.g. Pie Chart Options http://www.chartjs.org/docs/#doughnut-pie-chart-chart-options
        };

        $scope.onChartClick = function (event) {
            console.log(event);
        };
        
        $scope.majChart('min');
    }
]);


